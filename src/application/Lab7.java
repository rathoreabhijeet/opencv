package application;

import org.opencv.core.Core;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

public class Lab7 extends Application
{
	/**
	 * The main class for a JavaFX application. It creates and handles the main
	 * window with its resources (style, graphics, etc.).
	 * 
	 * This application looks for RED and GREEN balls in the camera video stream and
	 * plot circles around them according to their HSV values. 
	 * 
	 */
	Boolean screen = true;
	static int restart_number=0;
	
	@Override
	public void start(Stage primaryStage)
	{	
		
		
		try
		{
			// load the FXML resource
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("ObjRecognition.fxml"));
			// set a whitesmoke background
			root.setStyle("-fx-background-color: whitesmoke;");
			
//		    LineChart<Number,Number> lineChart = makeLineChart();
//			root.getChildren().add(ObjRecognitionController.graph_X_theta1);
			// create and style a scene
			Scene scene = new Scene(root, 1200, 800);
//			scene.setFill(null);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			// create the stage with the given title and the previously created
			// scene
			primaryStage.setTitle("Calibrate Balls");
			primaryStage.setScene(scene);
			// show the GUI
			primaryStage.show();
			primaryStage.setFullScreen(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		// load the native OpenCV library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		launch(args);
	}
}