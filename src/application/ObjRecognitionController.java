package application;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.beans.value.ChangeListener;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Toggle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.*;
import org.opencv.videoio.VideoCapture;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

/**
 * Class to control the main app
 * This class displays a live video stream with colored rectangles
 * User uses RED and GREEN balls to try and touch the rectangles
 * Once touched, rectangles disappear
 * Correct collision results in 1-up in score for either ball
 * There are two manipulator hands attached to the balls which move along with balls
 * Four plots keep track of elbow and shoulder angle variation of the two manipulators -
 * as they move around in the workspace. The graphs are saved when application closes.
 * Graphs show in real-time correct or incorrect collisions.
 */
public class ObjRecognitionController
{
	// FXML camera button
	@FXML
	private Button cameraButton;
	//FXML exit button
	@FXML
	private Button exitButton;
	//FXML exit button
	@FXML
	private Button restartButton;
	// the FXML area for showing the current frame
	@FXML
	private ImageView originalFrame;
	// the FXML area for showing the mask
	@FXML
	private ImageView maskImage;
	// the FXML area for showing the output of the morphological operations
	@FXML
	private ImageView morphImage;
	//Text and static components from the FXML
	@FXML
	private Text XLabel1;
	@FXML
	private Text YLabel1;
	@FXML
	private Text XLabel2;
	@FXML
	private Text YLabel2;
	@FXML
	private HBox BottomHBox;
	@FXML
	private HBox radioHBox;	
	// a timer for acquiring the video stream
	private Timer timer;
	// the OpenCV object that performs the video capture
	private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive;
	//Imageviews for legend
	@FXML
	private ImageView legend1;
	@FXML
	private ImageView legend2;
	@FXML
	private Label redCorrectCount;
	@FXML
	private Label greenCorrectCount;
	@FXML
	private RadioButton radioLeft;
	@FXML
	private RadioButton radioRight;
	@FXML
	private ToggleGroup radioGroup;
	
	
	Mat frame = new Mat();
	Mat faceFrame = new Mat();
	Mat graph_1 =new Mat(262, 350,CvType.CV_32SC3, new Scalar(180,180,180));
	Mat graph_2 =new Mat(262, 350,CvType.CV_32SC3, new Scalar(180,180,180));
	Mat graph_3 =new Mat(262, 350,CvType.CV_32SC3, new Scalar(180,180,180));
	Mat graph_4 =new Mat(262, 350,CvType.CV_32SC3, new Scalar(180,180,180));
	
	// property for object binding
	private ObjectProperty<Image> X_stream;
	private ObjectProperty<Image> Y_stream;
//	private ObjectProperty<String> hsvValuesProp;
	private ObjectProperty<String> redCorrectProp;
	private ObjectProperty<String> greenCorrectProp;
	Point final_center_red      = new Point();
	Point final_center_green      = new Point();
	int roundedRadius = 40;
	int link_length_1= 250;
	int link_length_2 = 250;
	Point[] red_link1_status = null;
	Point[] green_link1_status;
	double red_theta1=0;
	double red_theta2=0;
	double green_theta1=0;
	double green_theta2=0;
	Point origin_red = new Point(0,250);
	Point origin_green = new Point(640,250);
	Point red_link1_end = new Point();
	Point green_link1_end = new Point();
	Point red_ball_center = new Point();
	Point green_ball_center = new Point();
	int[] rect_to_draw = {1,1,1,1,1,1};
	boolean red_detect_status=false;
	boolean green_detect_status=false;
	
	//graph variables	
    Point graph1_origin1 = new Point(30,131);
    Point graph1_origin2 = new Point(30,131);
    Point graph2_origin1 = new Point(30,131);
    Point graph2_origin2 = new Point(30,131);
    Point graph3_origin1 = new Point(320,131);
    Point graph3_origin2 = new Point(320,131);
    Point graph4_origin1 = new Point(320,131);
    Point graph4_origin2 = new Point(320,131);
    
    //Color variables
    Scalar green = new Scalar(0,255,0);
	Scalar red = new Scalar(0,0,255);
	Scalar blue = new Scalar(255,0,0);
	Scalar white = new Scalar(255,255,255);
	Scalar black = new Scalar(0,0,0);
	Boolean collision = false;
	Boolean collision_correct = false;
	String redCorrectValues = "0";
	String greenCorrectValues = "0";
	String radio_toggle_status = "red";
	
	//excel file variables	
	long StartTime =0 ;
	long EndTime =0;
	String total_duration ="0";
	String[] collision_timestamp={"0","0","0","0","0","0"};
	int collision_counter =0;
	Boolean[] collision_status={false,false,false,false,false,false};
	
	ArrayList<Double> redBall_elbowAngles = new ArrayList<Double>();
	ArrayList<Double> redBall_shoulderAngles = new ArrayList<Double>();
	ArrayList<Double> greenBall_elbowAngles = new ArrayList<Double>();
	ArrayList<Double> greenBall_shoulderAngles = new ArrayList<Double>();
	ArrayList<Double> timestamp_array = new ArrayList<Double>();
	ArrayList<Point> lefteyeball_array = new ArrayList<Point>();
	ArrayList<Point> righteyeball_array = new ArrayList<Point>();

	
	DecimalFormat df = new DecimalFormat("#.#");
	
	// face cascade classifier
	private CascadeClassifier faceCascade;
	private CascadeClassifier eyeCascade;
	
	private int absoluteFaceSize;
	private int absoluteEyeSize;
		
	/**
	 * The action triggered by pushing the 'Exit' button on the GUI
	 */
	@FXML
	private void exitApp()
	{	
		Lab7.restart_number=Lab7.restart_number+1;
		writeExcelData();
		System.exit(0);
	}
	

	/**
	 * The action triggered by pushing the START button on the GUI
	 */
	@FXML
	private void startCamera()
	{	
		cameraButton.setVisible(false);
		//Set visibility of features other than main video stream
		BottomHBox.setVisible(true);
		radioHBox.setVisible(true);
		XLabel1.setVisible(true);
		XLabel2.setVisible(true);
		YLabel1.setVisible(true);
		YLabel2.setVisible(true);
		
		// Radio button settings
		radioLeft.setToggleGroup(radioGroup);
		radioRight.setToggleGroup(radioGroup);
		radioLeft.setSelected(true);
		radioLeft.setUserData("red");
		radioRight.setUserData("green");
		radioGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
		      public void changed(ObservableValue<? extends Toggle> ov,
		          Toggle old_toggle, Toggle new_toggle) {
		        if (radioGroup.getSelectedToggle() != null) {
		        	
		        	// sets the string value to RED or GREEN, to be used downstream in conditions
		        	radio_toggle_status= radioGroup.getSelectedToggle().getUserData().toString();
		        }
		      }
		    });
	
		//action set on GO TO APPLICATION button
		restartButton.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		        BorderPane root;
		        try {
		            root = FXMLLoader.load(getClass().getResource("ObjRecognition.fxml"));
		            Stage stage = new Stage();
		            stage.setTitle("Detect balls");
		            stage.setScene(new Scene(root, 1200, 800));
		            stage.show();
		            
		            //hide this current window
		            (((Node) event.getSource())).getScene().getWindow().hide();
		            timer.cancel();
					timer = null;
					cameraActive = false;
					// release the camera
					capture.release();
					Lab7.restart_number=Lab7.restart_number+1;
					writeExcelData();

		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		});
		
		// Set legend images - Elbow angles/Shoulder angles
    	final Image legend1_image = new Image(Lab7.class.getResourceAsStream("legend.png"));
    	legend1.setImage(legend1_image);
    	legend2.setImage(legend1_image);
    	
		// bind an image property with the original frame container
		final ObjectProperty<Image> imageProp = new SimpleObjectProperty<>();
		this.originalFrame.imageProperty().bind(imageProp);
		
		// bind an image property with the mask container
		X_stream = new SimpleObjectProperty<>();
		this.maskImage.imageProperty().bind(X_stream);
		
		// bind an image property with the container of the morph operators
		Y_stream = new SimpleObjectProperty<>();
		this.morphImage.imageProperty().bind(Y_stream);
		
		// bind an image property with the container containing the graphs
		redCorrectProp = new SimpleObjectProperty<>();
		this.redCorrectCount.textProperty().bind(redCorrectProp);
		greenCorrectProp = new SimpleObjectProperty<>();
		this.greenCorrectCount.textProperty().bind(greenCorrectProp);
		
		// set a fixed width for all the image to show and preserve image ratio
		// Main video stream
		this.imageViewProperties(this.originalFrame, 900);
		// Top graph stream
		this.imageViewProperties(this.maskImage, 350);
		//Bottom graph stream
		this.imageViewProperties(this.morphImage, 350);
		
		// Draw permanent axis features on graph_1
		Imgproc.line(graph_1, new Point(30,0), new Point(30,262), black);
		Imgproc.line(graph_1, new Point(0,131), new Point(350,131), black);
		Imgproc.line(graph_1, new Point(110,122), new Point(110,140), black);
		Imgproc.line(graph_1, new Point(190,122), new Point(190,140), black);
		Imgproc.line(graph_1, new Point(270,122), new Point(270,140), black);
		Imgproc.line(graph_1, new Point(350,122), new Point(350,140), black);
		Imgproc.line(graph_1, new Point(20,11), new Point(40,11), black);
		Imgproc.line(graph_1, new Point(20,251), new Point(40,251), black);
		//Text markers on graph_1 axis
		Imgproc.putText(graph_1, "-180", new Point(50,15), 0 , .4, black, 1);
		Imgproc.putText(graph_1, "180", new Point(50,255), 0 , .4, black, 1);
		Imgproc.putText(graph_1, "160", new Point(100,110), 0 , .4, black, 1);
		Imgproc.putText(graph_1, "320", new Point(180,110), 0 , .4, black, 1);
		Imgproc.putText(graph_1, "480", new Point(260,110), 0 , .4, black, 1);
		
		// Draw permanent axis features on graph_2
		Imgproc.line(graph_2, new Point(30,0), new Point(30,262), black);
		Imgproc.line(graph_2, new Point(0,131), new Point(350,131), black);
		Imgproc.line(graph_2, new Point(110,122), new Point(110,140), black);
		Imgproc.line(graph_2, new Point(190,122), new Point(190,140), black);
		Imgproc.line(graph_2, new Point(270,122), new Point(270,140), black);
		Imgproc.line(graph_2, new Point(350,122), new Point(350,140), black);
		Imgproc.line(graph_2, new Point(20,11), new Point(40,11), black);
		Imgproc.line(graph_2, new Point(20,251), new Point(40,251), black);
		//Text markers on graph_2 axis
		Imgproc.putText(graph_2, "-180", new Point(50,15), 0 , .4, black, 1);
		Imgproc.putText(graph_2, "180", new Point(50,255), 0 , .4, black, 1);
		Imgproc.putText(graph_2, "160", new Point(100,110), 0 , .4, black, 1);
		Imgproc.putText(graph_2, "320", new Point(180,110), 0 , .4, black, 1);
		Imgproc.putText(graph_2, "480", new Point(260,110), 0 , .4, black, 1);
		
		// Draw permanent axis features on graph_3
		Imgproc.line(graph_3, new Point(320,0), new Point(320,262), black);
		Imgproc.line(graph_3, new Point(0,131), new Point(350,131), black);
		Imgproc.line(graph_3, new Point(240,122), new Point(240,140), black);
		Imgproc.line(graph_3, new Point(160,122), new Point(160,140), black);
		Imgproc.line(graph_3, new Point(80,122), new Point(80,140), black);
		Imgproc.line(graph_3, new Point(350,122), new Point(350,140), black);
		Imgproc.line(graph_3, new Point(310,11), new Point(330,11), black);
		Imgproc.line(graph_3, new Point(310,251), new Point(330,251), black);
		//Text markers on graph_3 axis
		Imgproc.putText(graph_3, "-180", new Point(270,15), 0 , .4, black, 1);
		Imgproc.putText(graph_3, "180", new Point(270,255), 0 , .4, black, 1);
		Imgproc.putText(graph_3, "160", new Point(230,110), 0 , .4, black, 1);
		Imgproc.putText(graph_3, "320", new Point(150,110), 0 , .4, black, 1);
		Imgproc.putText(graph_3, "480", new Point(70,110), 0 , .4, black, 1);
		
		// Draw permanent axis features on graph_4
		Imgproc.line(graph_4, new Point(320,0), new Point(320,262), black);
		Imgproc.line(graph_4, new Point(0,131), new Point(350,131), black);
		Imgproc.line(graph_4, new Point(240,122), new Point(240,140), black);
		Imgproc.line(graph_4, new Point(160,122), new Point(160,140), black);
		Imgproc.line(graph_4, new Point(80,122), new Point(80,140), black);
		Imgproc.line(graph_4, new Point(350,122), new Point(350,140), black);
		Imgproc.line(graph_4, new Point(310,11), new Point(330,11), black);
		Imgproc.line(graph_4, new Point(310,251), new Point(330,251), black);
		//Text markers on graph_4 axis
		Imgproc.putText(graph_4, "-180", new Point(270,15), 0 , .4, black, 1);
		Imgproc.putText(graph_4, "180", new Point(270,255), 0 , .4, black, 1);
		Imgproc.putText(graph_4, "160", new Point(230,110), 0 , .4, black, 1);
		Imgproc.putText(graph_4, "320", new Point(150,110), 0 , .4, black, 1);
		Imgproc.putText(graph_4, "480", new Point(70,110), 0 , .4, black, 1);
				
		// set one digit decimal rounding
		df.setRoundingMode(RoundingMode.FLOOR);
		
		//Initialize face detection controller
		this.faceCascade = new CascadeClassifier();
		this.eyeCascade = new CascadeClassifier();
		this.absoluteFaceSize = 0;
		this.absoluteEyeSize = 0;
		
		//load classifiers
		this.faceCascade.load("resources/haarcascade_frontalface_alt.xml");
		this.eyeCascade.load("resources/haarcascade_eye_tree_eyeglasses.xml");
		
		if (!this.cameraActive)
		{
			// start the video capture
			this.capture.open(0);
			
			// is the video stream available?
			if (this.capture.isOpened())
			{
				this.cameraActive = true;
				
				//mark the start time
				StartTime = new Date().getTime();

				// grab a frame every 33 ms (30 frames/sec)
				TimerTask frameGrabber = new TimerTask() {
					@Override
					public void run()
					{
						// update the image property => update the frame
						// shown in the UI
						Image frame = grabFrame();
						// Main video stream put on separate thread
						onFXThread(imageProp, frame);
						
					}
				};
				this.timer = new Timer();
				this.timer.schedule(frameGrabber, 0, 33);
				
				// update the button content
				this.cameraButton.setText("Stop Camera");
			}
			else
			{
				// log the error
				System.err.println("Failed to open the camera connection...");
			}
		}
		else
		{
			// the camera is not active at this point
			this.cameraActive = false;
			// update again the button content
			this.cameraButton.setText("Start Camera");
			//Save graphs to files with timestamp
			Imgcodecs.imwrite("graph_1_"+new Date().getHours()+new Date().getMinutes()+".png", graph_1);
			Imgcodecs.imwrite("graph_2_"+new Date().getHours()+new Date().getMinutes()+".png", graph_2);
			Imgcodecs.imwrite("graph_3_"+new Date().getHours()+new Date().getMinutes()+".png", graph_3);
			Imgcodecs.imwrite("graph_4_"+new Date().getHours()+new Date().getMinutes()+".png", graph_4);
			
			//Hide the static label and components
			BottomHBox.setVisible(false);
			radioHBox.setVisible(false);
			XLabel1.setVisible(false);
			XLabel2.setVisible(false);
			YLabel1.setVisible(false);
			YLabel2.setVisible(false);
			maskImage.setVisible(false);
			morphImage.setVisible(false);
			legend1.setVisible(false);
			legend2.setVisible(false);
			
			//mark the end time and get the total duration
			EndTime = new Date().getTime();
			total_duration = df.format((EndTime-StartTime)/1000.0);
			
			writeExcelData();
		}
	}
	
	/**
	 * Get a frame from the opened video stream (if any)
	 * 
	 */	

	private Image grabFrame()
	{
		// init everything
		Image imageToShow = null;
		
		
		// check if the capture is open
		if (this.capture.isOpened())
		{
			try
			{
				// read the current frame
				this.capture.read(frame);
				faceFrame=frame;
				// if the frame is not empty, process it
				if (!frame.empty())
				{
					// init
					Mat blurredImage = new Mat();
					Mat hsvImage = new Mat();
					Mat mask_red = new Mat();
					Mat mask_green = new Mat();
					Mat morphOutput_red = new Mat();
					Mat morphOutput_green = new Mat();
					
					// remove some noise
					Imgproc.blur(frame, blurredImage, new Size(7, 7));
					
					// convert the frame to HSV
					Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);
					
					// threshold HSV image to select balls
//					Core.inRange(hsvImage, CalibrationController2.minValues_red, CalibrationController2.maxValues_red, mask_red);
//					Core.inRange(hsvImage, CalibrationController2.minValues_green, CalibrationController2.maxValues_green, mask_green);
					Core.inRange(hsvImage, new Scalar(0,110,20), new Scalar(5,230,255), mask_red);
					Core.inRange(hsvImage, new Scalar(42,47,20), new Scalar(83,255,144), mask_green);
					
					// morphological operators
					// dilate with large element, erode with small ones
					Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
					Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12, 12));
					
					Imgproc.erode(mask_red, morphOutput_red, erodeElement);
					Imgproc.erode(mask_red, morphOutput_red, erodeElement);
					
					Imgproc.dilate(mask_red, morphOutput_red, dilateElement);
					Imgproc.dilate(mask_red, morphOutput_red, dilateElement);
					
					Imgproc.erode(mask_green, morphOutput_green, erodeElement);
					Imgproc.erode(mask_green, morphOutput_green, erodeElement);
					
					Imgproc.dilate(mask_green, morphOutput_green, dilateElement);
					Imgproc.dilate(mask_green, morphOutput_green, dilateElement);
														
					// find the ball(s) contours and show them
					frame = this.findAndDrawRedBalls(morphOutput_red, frame);
					frame = this.findAndDrawGreenBalls(morphOutput_green, frame);
					
					// detectCollision sends an array of 1s & 0s for the rectangles to be drawn
					// depending upon which rectangles are touched
					rect_to_draw = detectCollision(final_center_red,final_center_green);
					
					// draw rectangles as output by detectCollision function
					drawRectangles(frame,red,green,rect_to_draw,0);
					
					// set one digit decimal rounding
					df.setRoundingMode(RoundingMode.FLOOR);
					
					//Draw the manipulator links
					if(red_detect_status){
						
						// invKinematics outputs link_1 end-point coordinates and both angles
						red_link1_status = invKinematics(red_ball_center,origin_red);
						red_link1_end= red_link1_status[0];
						red_theta1 = red_link1_status[1].x;
						red_theta2 = red_link1_status[1].y;
						
						// Draw lines to show manipulator links
						Imgproc.line(frame, red_link1_end, origin_red, blue);
						Imgproc.line(frame, red_link1_end, red_ball_center, blue);
						
						// graph_1 and graph_2 get scaled down inputs from above outputs
						graph_1 = makeGraph1(red_ball_center.x,red_link1_status[1].x,red_link1_status[1].y);
						graph_2 = makeGraph2(red_ball_center.y,red_link1_status[1].x,red_link1_status[1].y);
						if(green_detect_status==false){
							collision=false;
							}
					}					
					
					if(green_detect_status){
						
						// invKinematics outputs link_1 end-point coordinates and both angles
						green_link1_status = invKinematics(green_ball_center,origin_green);
						green_link1_end= green_link1_status[0];
						green_theta1 = green_link1_status[1].x;
						green_theta2 = green_link1_status[1].y;
						
						// Draw lines to show manipulator links
						Imgproc.line(frame, green_link1_end, origin_green, blue);
						Imgproc.line(frame, green_link1_end, green_ball_center, blue);
						
						// graph_3 and graph_4 get scaled down inputs from above outputs
						graph_3 = makeGraph3(green_ball_center.x,green_link1_status[1].x,green_link1_status[1].y);
						graph_4 = makeGraph4(green_ball_center.y,green_link1_status[1].x,green_link1_status[1].y);
						collision=false;
					}
					
					// toggle graphs to RED
					if(red_detect_status==true && radio_toggle_status=="red" ){
							this.onFXThread(X_stream, this.mat2Image(graph_1));
							this.onFXThread(Y_stream, this.mat2Image(graph_2));
							red_detect_status = false;
					}
					
					// toggle graphs to GREEN
					else if(green_detect_status==true && radio_toggle_status=="green"){
							this.onFXThread(X_stream, this.mat2Image(graph_3));
							this.onFXThread(Y_stream, this.mat2Image(graph_4));
							green_detect_status = false;
					}
										
					//store angle information in arraylist
					redBall_elbowAngles.add(round((57.32*red_theta1),1));
					redBall_shoulderAngles.add(round((57.32*red_theta2),1));
					timestamp_array.add((double) (new Date().getTime()-StartTime)/1000);
					greenBall_elbowAngles.add(round(57.32*green_theta1,1));
					greenBall_shoulderAngles.add(round(57.32*green_theta2,1));
					
					//Get eyeball location
					lefteyeball_array.add(detectFace(faceFrame,0));
					righteyeball_array.add(detectFace(faceFrame,1));
					
					// Correct collision counts put on separate threads
					this.onFXThread(redCorrectProp, redCorrectValues);
					this.onFXThread(greenCorrectProp, greenCorrectValues);
					
					// convert the Mat object (OpenCV) to Image (JavaFX)
					
					imageToShow = mat2Image(frame);
					
				}
				
			}
			catch (Exception e)
			{
				// log the (full) error
				System.err.print("ERROR");
				e.printStackTrace();
			}
		}
		
		return imageToShow;
	}
	
	/**
	 * Function takes input as the thresholded binary Mat which shows RED area as white
	 * Finds contours in the binary image
	 * By defaults largest contour is for the ball if colors properly thresholded
	 * A circle is drawn on the LIVE frame, enclosing the largest contour
	 */	
	private Mat findAndDrawRedBalls(Mat maskedImage, Mat frame)
	{
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		int largestCount=0;
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

	    Point center      = new Point();
	    float[] radius     = new float[contours.size()];
	    
	    double area;
	    double prev_area=0;
	   	//if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			red_detect_status=true;

				for (int i = 0; i < contours.size(); i++) {
					//Detect circles with inbuilt function
			        area = Imgproc.contourArea(new MatOfPoint(contours.get(i)));
			        if (area > prev_area){
			        	prev_area = area;
			        	largestCount = i;
			        	final_center_red = center;
			        }
			        
			    }
				prev_area=0;
		        Imgproc.minEnclosingCircle(new MatOfPoint2f(contours.get(largestCount).toArray()), center, radius);
		        Imgproc.circle(frame, new Point(final_center_red.x,final_center_red.y), roundedRadius, red,2);
				
		}
		
//		Imgproc.putText(frame, touch_status1, new Point(50,450), 0 , 1, new Scalar(0,200,200), 2);
		red_ball_center.x = final_center_red.x;
		red_ball_center.y = final_center_red.y;
		return frame;
	}
	
	/**
	 * Function takes input as the thresholded binary Mat which shows GREEN area as white
	 * Finds contours in the binary image
	 * By defaults largest contour is for the ball if colors properly thresholded
	 * A circle is drawn on the LIVE frame, enclosing the largest contour
	 */
	private Mat findAndDrawGreenBalls(Mat maskedImage, Mat frame)
	{
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		int largestCount=0;
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

	    Point center      = new Point();
	    float[] radius     = new float[contours.size()];
	    double area;
	    double prev_area=0;
	   	//if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			green_detect_status=true;

				for (int i = 0; i < contours.size(); i++) {
					//Detect circles with inbuilt function
			        area = Imgproc.contourArea(new MatOfPoint(contours.get(i)));
			        if (area > prev_area){
			        	prev_area = area;
			        	largestCount = i;
			        	final_center_green = center;
			        }
			        
			    }
				prev_area=0;
		        Imgproc.minEnclosingCircle(new MatOfPoint2f(contours.get(largestCount).toArray()), center, radius);
				Imgproc.circle(frame, new Point(final_center_green.x,final_center_green.y), roundedRadius,green,2);
						
		}
		green_ball_center.x = final_center_green.x;
		green_ball_center.y = final_center_green.y;
	
		return frame;
	}
	
	private void imageViewProperties(ImageView image, int dimension)
	{
		// set a fixed width for the given ImageView
		image.setFitWidth(dimension);
		// preserve the image ratio
		image.setPreserveRatio(true);
	}

	/**
	 * 	
	 * @param frame - Live stream
	 * @return - Image to be shown with JavaFX
	 */
	private Image mat2Image(Mat frame)
	{
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		// encode the frame in the buffer, according to the PNG format
		Imgcodecs.imencode(".png", frame, buffer);
		// build and return an Image created from the image encoded in the
		// buffer
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}
	
	/**
	 * 
	 * @param property
	 * @param value
	 * Sets an object on a thread
	 */
	private <T> void onFXThread(final ObjectProperty<T> property, final T value)
	{
		Platform.runLater(new Runnable() {
			
			@Override
			public void run()
			{
				property.set(value);
				
			}
		});
	}
	
	/**
	 * 
	 * @param EF - Coordinates of RED or GREEN ball center coords.
	 * @param origin - Point where the manipulator is FIXED to the screen
	 * @return - Coords of link_1 end and both manipulator angles
	 * Uses basic inverse kinematics for 2-linked planar manipulator
	 * Singularity is avoided, only one configuration is being used
	 */
	private Point[] invKinematics(Point EF, Point origin){
		Point[] EF_pos = {new Point(0,0),new Point(0,0)};
		Point EF_mod = new Point(EF.x-origin.x,origin.y-EF.y);
		
		double c2 =(Math.pow(EF_mod.x, 2)+Math.pow(EF_mod.y, 2)-Math.pow(link_length_1, 2)
				-Math.pow(link_length_2, 2))/(2*link_length_1*link_length_2);
		double s2_1 = Math.sqrt(1-Math.pow(c2, 2));
//		double s2_2 = -Math.sqrt(1-Math.pow(c2, 2));
		double c1_1 = ((link_length_1+link_length_2*c2)*(EF_mod.x)+link_length_2*s2_1*(EF_mod.y))
				/(Math.pow(link_length_1, 2)+Math.pow(link_length_2, 2)+2*link_length_1*link_length_2*c2);
		double s1_1 = ((link_length_1+link_length_2*c2)*EF_mod.y-link_length_2*s2_1*EF_mod.x)
				/(Math.pow(link_length_1, 2)+Math.pow(link_length_2, 2)+2*link_length_1*link_length_2*c2);
//		double c1_2 = ((link_length_1+link_length_2*c2)*(EF_mod.x)+link_length_2*s2_2*(EF_mod.y))
//				/(Math.pow(link_length_1, 2)+Math.pow(link_length_2, 2)+2*link_length_1*link_length_2*c2);
//		double s1_2 = ((link_length_1+link_length_2*c2)*EF_mod.y-link_length_2*s2_2*EF_mod.x)
//				/(Math.pow(link_length_1, 2)+Math.pow(link_length_2, 2)+2*link_length_1*link_length_2*c2);
		double EF_theta2_1 =  Math.atan2(s2_1, c2);
//		double EF_theta2_2 =  Math.atan2(s2_2, c2);
//
		double EF_theta1_1 =  Math.atan2(s1_1, c1_1);
//		double EF_theta1_2 =  Math.atan2(s1_2, c1_2);
		double EF_theta ;
		
		// since link_2 follows the ball, the length is allowed to vary, hence COS(angle) can go above 1
		if (c2>=1 || c2<=-1){
		EF_theta = Math.atan2(EF_mod.y,EF_mod.x);
		EF_pos[0].x =  (origin.x+ link_length_1*Math.cos(EF_theta));
		EF_pos[0].y =  (origin.y-link_length_1*Math.sin(EF_theta));
		EF_pos[1].x = EF_theta;
		EF_pos[1].y = 0;
		}
		else{
		EF_pos[0].x =  (origin.x+ link_length_1*c1_1);
		EF_pos[0].y =  (origin.y-link_length_1*s1_1);
		EF_pos[1].x = EF_theta1_1;
		EF_pos[1].y = EF_theta2_1;
		}
				
		return EF_pos;
	}
	
	/**
	 * @param red_center - Coords of RED ball center
	 * @param green_center - Coords of GREEN ball center
	 * @return - Returns array of 1s & 0s to indicate which rectangles have collided
	 * The function calculates a bitwise_AND of binary frames with rectangles and balls
	 * Wherever overlap occurs, the AND operation outputs 1
	 */
	private int[] detectCollision(Point red_center, Point green_center){
		// set one digit decimal rounding
		df.setRoundingMode(RoundingMode.FLOOR);
				
		Mat binary_red= new Mat(frame.rows(), frame.cols(),CvType.CV_8UC1, black);
		Mat binary_green= new Mat(frame.rows(), frame.cols(),CvType.CV_8UC1, black);
		Mat binary_rect= new Mat(frame.rows(), frame.cols(),CvType.CV_8UC1, black);

		// draw white rectangles at designated locations
		drawRectangles(binary_rect,white,white, rect_to_draw,0.4);
		
		//draw circles at ball locations
		Imgproc.circle(binary_red, new Point(final_center_red.x,final_center_red.y), 
				roundedRadius, white,2);
		Imgproc.circle(binary_green, new Point(final_center_green.x,final_center_green.y), 
				roundedRadius, white,2);
		
		//bitwise_AND the MATs
		Core.bitwise_and(binary_rect, binary_red, binary_red );
		Core.bitwise_and(binary_rect, binary_green, binary_green );
		
		Mat idx = Mat.zeros(binary_red.size(),CvType.CV_8UC1); 
		Mat idx1 = Mat.zeros(binary_green.size(),CvType.CV_8UC1); 
		
		//detect non-zero pixels for RED (where overlap happens)
		if (Core.countNonZero(binary_red)>0 ){
			Core.findNonZero(binary_red, idx);
			collision_timestamp[collision_counter] = df.format((double)(new Date().getTime()-StartTime)/1000);
			collision_counter=collision_counter+1;
			
			//update to-be-drawn-rectangles
			rect_to_draw[findTouchedRectangle(idx.get(0,0)[0],idx.get(0,0)[1],red)] = 0;
			collision_status[collision_counter-1]=collision_correct;
			collision =true;
		}
		
		//detect non-zero pixels for GREEN (where overlap happens)
		if (Core.countNonZero(binary_green)>0 ){
			Core.findNonZero(binary_green, idx1);
			collision_timestamp[collision_counter] = df.format((double)(new Date().getTime()-StartTime)/1000);
			collision_counter=collision_counter+1;
			
			//update to-be-drawn-rectangles
			rect_to_draw[findTouchedRectangle(idx1.get(0,0)[0],idx1.get(0,0)[1], green)] = 0;
			collision_status[collision_counter-1]=collision_correct;
			collision =true;
		}
		
		return rect_to_draw;
		
	}
	
	/**
	 * @param frame - LIVE frame
	 * @param color1 - first color
	 * @param color2 - second color
	 * @param drawIndex - array to indicate which rectangles to draw
	 */
	private void drawRectangles(Mat frame,Scalar color1, Scalar color2, int[] drawIndex,double perc_margin){
			
			if(drawIndex[0]==1)
				{
				Imgproc.rectangle( frame, new Point( (int)(160+perc_margin*80),(int)(150+perc_margin*80) ),new Point( (int)(240-perc_margin*80),(int)(230-perc_margin*80)),color1, -1 );
				
				}

			if(drawIndex[1]==1)
				{Imgproc.rectangle( frame, new Point( (int)(280+perc_margin*80),(int)(150+perc_margin*80) ),new Point( (int)(360-perc_margin*80),(int)(230-perc_margin*80)),color2, -1 );}

			if(drawIndex[2]==1)
				{Imgproc.rectangle( frame, new Point( (int)(400+perc_margin*80),(int)(150+perc_margin*80)),new Point( (int)(480-perc_margin*80),(int)(230-perc_margin*80)),color1, -1 );}

			if(drawIndex[3]==1)
				{Imgproc.rectangle( frame, new Point( (int)(160+perc_margin*80),(int)(270+perc_margin*80) ),new Point( (int)(240-perc_margin*80),(int)(350-perc_margin*80)),color2, -1 );}

			if(drawIndex[4]==1)
				{Imgproc.rectangle( frame, new Point( (int)(280+perc_margin*80),(int)(270+perc_margin*80) ),new Point( (int)(360-perc_margin*80),(int)(350-perc_margin*80)),color1, -1 );}

			if(drawIndex[5]==1)
				{Imgproc.rectangle( frame, new Point( (int)(400+perc_margin*80),(int)(270+perc_margin*80) ),new Point( (int)(480-perc_margin*80),(int)(350-perc_margin*80)),color2, -1 );}

	}
	
	/**
	 * @param x 
	 * @param y
	 * @param ballColor
	 * @return
	 * If detectcollision returns a positive on collision, this function finds out which 
	 * rectangle has collided
	 * Updates the correct collisions for respective balls
	 */
	private int findTouchedRectangle(double x,double y,Scalar ballColor){
		int rectangleTouched;
		if (ballColor==red){
			if(y<=250){
				if (x<=260){
				rectangleTouched=1;
				collision_correct=true;
				redCorrectValues=Integer.toString((Integer.parseInt(redCorrectValues)+1));
				}
				else if (x>=380){
				rectangleTouched=3;
				collision_correct=true;
				redCorrectValues=Integer.toString((Integer.parseInt(redCorrectValues)+1));
				}
				else {
				rectangleTouched=2;
				collision_correct=false;
				}
			}
			else{
				if (x<=260){
				rectangleTouched=4;
				collision_correct=false;
				}
				else if (x>=380){
				rectangleTouched=6;
				collision_correct=false;
				}
				else {
				rectangleTouched=5;
				collision_correct=true;
				redCorrectValues=Integer.toString((Integer.parseInt(redCorrectValues)+1));
				}
			}
		}
		else {
			if(y<=250){
				if (x<=260){
				rectangleTouched=1;
				collision_correct=false;
				}
				else if (x>=380){
				rectangleTouched=3;
				collision_correct=false;
				}
				else {
				rectangleTouched=2;
				collision_correct=true;
				greenCorrectValues=Integer.toString((Integer.parseInt(greenCorrectValues)+1));
				}
			}
			else{
				if (x<=260){
				rectangleTouched=4;
				collision_correct=true;
				greenCorrectValues=Integer.toString((Integer.parseInt(greenCorrectValues)+1));
				}
				else if (x>=380){
				rectangleTouched=6;
				collision_correct=true;
				greenCorrectValues=Integer.toString((Integer.parseInt(greenCorrectValues)+1));
				}
				else {
				rectangleTouched=5;
				collision_correct=false;
				}
			}
		}
		
		return (rectangleTouched-1);
	}
	
	/**
	 * @param x - X axis parameter
	 * @param angle1 - Angle parameter for RED ball
	 * @param angle2 - Angle parameter for GREEN ball
	 * @return
	 */
	private Mat makeGraph1(double x, double angle1, double angle2){
				
		//scaling function to fit the video stream workspace into graph workspace
		Point transformed_angle1 = new Point(x/2+30,-angle1*(2.0/3)*(180.0/3.14)+131);
		Point transformed_angle2 = new Point(x/2+30,angle2*(2.0/3)*(180.0/3.14)+131);

		//markers on graph for correct or incorrect collisions
		if(collision==true){
			if (collision_correct==true){
				Imgproc.circle(graph_1, transformed_angle1, 5, green,-1);
				Imgproc.circle(graph_1, transformed_angle2, 5, green,-1);
			}
			else {
				Imgproc.circle(graph_1, transformed_angle1, 5, red,-1);
				Imgproc.circle(graph_1, transformed_angle2, 5, red,-1);
			}
		}
		
		Imgproc.line(graph_1, transformed_angle1, graph1_origin1, blue);
		Imgproc.line(graph_1, transformed_angle2, graph1_origin2, green);
		graph1_origin1 = transformed_angle1;
		graph1_origin2 = transformed_angle2;
		
		return graph_1;
		
	}
	
	/**
	 * @param y - X axis parameter (Y axis of actual video stream)
	 * @param angle1 - Angle parameter for RED ball
	 * @param angle2 - Angle parameter for GREEN ball
	 * @return
	 */
	private Mat makeGraph2(double y, double angle1, double angle2){
		
		//scaling function to fit the video stream workspace into graph workspace
		Point transformed_angle1 = new Point(y*2.0/3+30,-angle1*(2.0/3)*(180.0/3.14)+131);
		Point transformed_angle2 = new Point(y*2.0/3+30,angle2*(2.0/3)*(180.0/3.14)+131);
		
		//markers on graph for correct or incorrect collisions
		if(collision==true){
			if (collision_correct==true){
				Imgproc.circle(graph_2, transformed_angle1, 5, green,-1);
				Imgproc.circle(graph_2, transformed_angle2, 5, green,-1);
			}
			else {
				Imgproc.circle(graph_2, transformed_angle1, 5, red,-1);
				Imgproc.circle(graph_2, transformed_angle2, 5, red,-1);
			}
//			collision=false;
		}
		Imgproc.line(graph_2, transformed_angle1, graph2_origin1, blue);
		Imgproc.line(graph_2, transformed_angle2, graph2_origin2, green);
		graph2_origin1 = transformed_angle1;
		graph2_origin2 = transformed_angle2;

		return graph_2;
		
	}
	
	/**
	 * @param x - X axis parameter
	 * @param angle1 - Angle parameter for RED ball
	 * @param angle2 - Angle parameter for GREEN ball
	 * @return
	 */
	private Mat makeGraph3(double x, double angle1, double angle2){
		
		//scaling function to fit the video stream workspace into graph workspace
		Point transformed_angle1 = new Point(320-x/2,angle1*(2.0/3)*(180.0/3.14)+131);
		Point transformed_angle2 = new Point(320-x/2,-angle2*(2.0/3)*(180.0/3.14)+131);
		
		//markers on graph for correct or incorrect collisions
		if(collision==true){
			if (collision_correct==true){
				Imgproc.circle(graph_3, transformed_angle1, 5, green,-1);
				Imgproc.circle(graph_3, transformed_angle2, 5, green,-1);
			}
			else {
				Imgproc.circle(graph_3, transformed_angle1, 5, red,-1);
				Imgproc.circle(graph_3, transformed_angle2, 5, red,-1);
			}
		}
		Imgproc.line(graph_3, transformed_angle1, graph3_origin1, blue);
		Imgproc.line(graph_3, transformed_angle2, graph3_origin2, green);
		graph3_origin1 = transformed_angle1;
		graph3_origin2 = transformed_angle2;

		return graph_3;
		
	}
	
	/**
	 * @param y - X axis parameter (Y axis of actual video stream)
	 * @param angle1 - Angle parameter for RED ball
	 * @param angle2 - Angle parameter for GREEN ball
	 * @return
	 */
	private Mat makeGraph4(double y, double angle1, double angle2){
	
		//scaling function to fit the video stream workspace into graph workspace
		Point transformed_angle1 = new Point(y*2.0/3+30,angle1*(2.0/3)*(180.0/3.14)+131);
		Point transformed_angle2 = new Point(y*2.0/3+30,-angle2*(2.0/3)*(180.0/3.14)+131);
	
		//markers on graph for correct or incorrect collisions
		if(collision==true){
			if (collision_correct==true){
				Imgproc.circle(graph_4, transformed_angle1, 5, green,-1);
				Imgproc.circle(graph_4, transformed_angle2, 5, green,-1);
			}
			else {
				Imgproc.circle(graph_4, transformed_angle1, 5, red,-1);
				Imgproc.circle(graph_4, transformed_angle2, 5, red,-1);
			}
	//		collision=false;
		}
		Imgproc.line(graph_4, transformed_angle1, graph4_origin1, blue);
		Imgproc.line(graph_4, transformed_angle2, graph4_origin2, green);
		graph4_origin1 = transformed_angle1;
		graph4_origin2 = transformed_angle2;
	
		return graph_4;
		
}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public void writeExcelData(){
		//write data to excel sheet
		WriteExcel.writeData(collision_timestamp,collision_status,total_duration,timestamp_array,
				redBall_elbowAngles,redBall_shoulderAngles,greenBall_elbowAngles,greenBall_shoulderAngles,
				redCorrectValues,greenCorrectValues,lefteyeball_array,righteyeball_array);
		
		// stop the timer
		if (this.timer != null)
		{
			this.timer.cancel();
			this.timer = null;
		}
		// release the camera
		this.capture.release();
	}
	
	private Point detectFace(Mat faceFrame, int index)
	{
		index=0;
		MatOfRect faces = new MatOfRect();
		MatOfRect eyes = new MatOfRect();
		Mat grayFrame = new Mat();
		Point eye = new Point();
		
		// convert the frame in gray scale
		Imgproc.cvtColor(faceFrame, grayFrame, Imgproc.COLOR_BGR2GRAY);
		// equalize the frame histogram to improve the result
		Imgproc.equalizeHist(grayFrame, grayFrame);
		
		// compute minimum face size (20% of the frame height, in our case)
		if (this.absoluteFaceSize == 0)
		{
			int height = grayFrame.rows();
			if (Math.round(height * 0.2f) > 0)
			{
				this.absoluteFaceSize = Math.round(height * 0.2f);
				this.absoluteEyeSize = Math.round(height * 0.05f);
			}
		}
		
		// detect faces
		this.faceCascade.detectMultiScale(grayFrame, faces, 1.1, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE,
				new Size(this.absoluteFaceSize, this.absoluteFaceSize), new Size());
				
		// each rectangle in faces is a face: draw them!
		Rect[] facesArray = faces.toArray();
		for (int i = 0; i < facesArray.length; i++){
//			Imgproc.rectangle(faceFrame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 3);
			
			Rect roi = new Rect(facesArray[i].tl(), facesArray[i].br());
			Mat imageROI = faceFrame.submat(roi);
			this.eyeCascade.detectMultiScale(imageROI, eyes, 1.1, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE,
					new Size(this.absoluteEyeSize, this.absoluteEyeSize), new Size());
			
			Rect[] eyesArray = eyes.toArray();
		     
			if(eyesArray.length!=0){
			eye = new Point( facesArray[index].x + eyesArray[index].x + eyesArray[index].width*0.5, facesArray[index].y + eyesArray[index].y + eyesArray[index].height*0.5 );
			}
//		       int radius = (int) Math.round( (eyesArray[j].width + eyesArray[j].height)*0.25 );		       
//		       Imgproc.circle( frame, center, 1, new Scalar( 255, 0, 0 ), 4, 8, 0 );
		     
		}
		return eye;
	}
	
}