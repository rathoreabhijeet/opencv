package application;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.opencv.core.Point;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class WriteExcel {
	
    public static String FILE_PATH = "writeData_restart-0"+".xls";
    
    //We are making use of a single instance to prevent multiple write access to same file.
    private static final WriteExcel INSTANCE = new WriteExcel();

    public static WriteExcel getInstance() {
        return INSTANCE;
    }

    public WriteExcel() {
    }

    
    
    public static void writeData (String[] collision_timestamp, Boolean[] collision_status, String duration,
    		ArrayList<Double> timestamp_array, ArrayList<Double> redBall_elbowAngles, ArrayList<Double> redBall_shoulderAngles,
    		ArrayList<Double> greenBall_elbowAngles, ArrayList<Double> greenBall_shoulderAngles,
    		String redCorrectValues, String greenCorrectValues,ArrayList<Point> lefteyeball_array,ArrayList<Point> righteyeball_array){
    	
    	FILE_PATH = "writeData_restart-"+Lab7.restart_number+".xls";        

        Workbook workbook = new HSSFWorkbook();
        Sheet summarySheet = workbook.createSheet("Summary");

        // write static information        	       
        Row row1 = summarySheet.createRow(1);
        row1.createCell(1).setCellValue("TIMESTAMPS");

        Row row3 = summarySheet.createRow(3);
        row3.createCell(1).setCellValue("Total runtime for application");

        Row row4 = summarySheet.createRow(4);
        row4.createCell(2).setCellValue("seconds");

        int rowindex = 6;
        Row row6 = summarySheet.createRow(rowindex);
        row6.createCell(1).setCellValue("Collision 1 @");
        Row row7 = summarySheet.createRow(rowindex++);
        row7.createCell(2).setCellValue("seconds");
        Row row8 = summarySheet.createRow(rowindex++);
        row8.createCell(2).setCellValue("collision");

        rowindex = 10;
        Row row10 = summarySheet.createRow(rowindex);
        row10.createCell(1).setCellValue("Collision 2 @");
        Row row11 = summarySheet.createRow(rowindex++);
        row11.createCell(2).setCellValue("seconds");
        Row row12 = summarySheet.createRow(rowindex++);
        row12.createCell(2).setCellValue("collision");
        
        rowindex = 14;
        Row row14 = summarySheet.createRow(rowindex);
        row14.createCell(1).setCellValue("Collision 3 @");
        Row row15 = summarySheet.createRow(rowindex++);
        row15.createCell(2).setCellValue("seconds");
        Row row16 = summarySheet.createRow(rowindex++);
        row16.createCell(2).setCellValue("collision");
        
        rowindex = 18;
        Row row18 = summarySheet.createRow(rowindex);
        row18.createCell(1).setCellValue("Collision 4 @");
        Row row19 = summarySheet.createRow(rowindex++);
        row19.createCell(2).setCellValue("seconds");
        Row row20 = summarySheet.createRow(rowindex++);
        row20.createCell(2).setCellValue("collision");
        
        rowindex = 22;
        Row row22 = summarySheet.createRow(rowindex);
        row22.createCell(1).setCellValue("Collision 5 @");
        Row row23 = summarySheet.createRow(rowindex++);
        row23.createCell(2).setCellValue("seconds");
        Row row24 = summarySheet.createRow(rowindex++);
        row24.createCell(2).setCellValue("collision");
        
        rowindex = 26;
        Row row26 = summarySheet.createRow(rowindex);
        row26.createCell(1).setCellValue("Collision 6 @");
        Row row27 = summarySheet.createRow(rowindex++);
        row27.createCell(2).setCellValue("seconds");
        Row row28 = summarySheet.createRow(rowindex++);
        row28.createCell(2).setCellValue("collision");
        
        
        Row row31 = summarySheet.createRow(31);
        row31.createCell(1).setCellValue("Correct Collisions RED");
        Row row32 = summarySheet.createRow(32);
        row32.createCell(1).setCellValue(redCorrectValues);
        
        Row row33 = summarySheet.createRow(33);
        row33.createCell(1).setCellValue("Correct Collisions GREEN");
        Row row34 = summarySheet.createRow(34);
        row34.createCell(1).setCellValue(greenCorrectValues);
   
        // write app data
        row7.createCell(1).setCellValue(collision_timestamp[0]);
        row11.createCell(1).setCellValue(collision_timestamp[1]);
        row15.createCell(1).setCellValue(collision_timestamp[2]);
        row19.createCell(1).setCellValue(collision_timestamp[3]);
        row23.createCell(1).setCellValue(collision_timestamp[4]);
        row27.createCell(1).setCellValue(collision_timestamp[5]);
        
        row8.createCell(1).setCellValue(collision_status[0]);
        row12.createCell(1).setCellValue(collision_status[1]);
        row16.createCell(1).setCellValue(collision_status[2]);
        row20.createCell(1).setCellValue(collision_status[3]);
        row24.createCell(1).setCellValue(collision_status[4]);
        row28.createCell(1).setCellValue(collision_status[5]);
      
        
        //total duration
        row4.createCell(1).setCellValue(duration);
        
        //Angle and time data - Sheet 2
        Sheet dataSheet = workbook.createSheet("Angles");
        Row row2_1 = dataSheet.createRow(0);
        row2_1.createCell(0).setCellValue("Time (s)");
        row2_1.createCell(1).setCellValue("RED Elbow Angle (Deg)");
        row2_1.createCell(2).setCellValue("RED Shoulder Angle (Deg)");
        row2_1.createCell(3).setCellValue("GREEN Elbow Angle (Deg)");
        row2_1.createCell(4).setCellValue("GREEN Shoulder Angle (Deg)");
        
        for (int i=1;i<timestamp_array.size();i++){
        	
        	Row row = dataSheet.createRow(i);
        	row.createCell(0).setCellValue((double) timestamp_array.get(i));
        	row.createCell(1).setCellValue((double) redBall_elbowAngles.get(i));
        	row.createCell(2).setCellValue((double) redBall_shoulderAngles.get(i));
        	row.createCell(3).setCellValue((double) greenBall_elbowAngles.get(i));
        	row.createCell(4).setCellValue((double) greenBall_shoulderAngles.get(i));
        }
        
      //Eyeball movement Data - Sheet 3
        Sheet trackSheet = workbook.createSheet("Tracking");
        Row row2_2 = trackSheet.createRow(0);
        row2_2.createCell(0).setCellValue("Time (s)");
        row2_2.createCell(1).setCellValue("Left Eye (X)");
        row2_2.createCell(2).setCellValue("Left Eye (Y)");
        row2_2.createCell(3).setCellValue("Right Eye (X)");
        row2_2.createCell(4).setCellValue("Right Eye (Y)");
        
        for (int i=1;i<lefteyeball_array.size();i++){
        	
        	Row row = trackSheet.createRow(i);        
        	row.createCell(0).setCellValue((double) timestamp_array.get(i));
           	row.createCell(1).setCellValue((double) lefteyeball_array.get(i).x);        	
        	row.createCell(2).setCellValue((double) lefteyeball_array.get(i).y);
        	row.createCell(3).setCellValue((double) righteyeball_array.get(i).x);
        	row.createCell(4).setCellValue((double) righteyeball_array.get(i).y);
        }
        
        //write this workbook in excel file.
        try {
            FileOutputStream fos = new FileOutputStream(FILE_PATH);
            workbook.write(fos);
            fos.close();

            System.out.println(FILE_PATH + " is successfully written");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    

}
