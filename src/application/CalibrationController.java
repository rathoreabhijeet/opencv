package application;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.beans.value.ChangeListener;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Toggle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.*;
import org.opencv.videoio.VideoCapture;
import org.opencv.imgproc.Imgproc;

/**
 * The controller associated with the only view of our application. The
 * application logic is implemented here. It handles the button for
 * starting/stopping the camera, the acquired video stream, the relative
 * controls and the image segmentation process.
 * 
 * @author <a href="mailto:luigi.derussis@polito.it">Luigi De Russis</a>
 * @since 2015-01-13
 * 
 */
public class CalibrationController
{
	// FXML camera button
	@FXML
	private Button calibrateButton;
	@FXML
	private Button goToApp;
	// the FXML area for showing the current frame
	@FXML
	private ImageView originalFrame;
	//Text and static components from the FXML
	@FXML
	private TextField HSV_red_min;
	@FXML
	private TextField HSV_red_max;
	@FXML
	private TextField HSV_green_min;
	@FXML
	private TextField HSV_green_max;
	@FXML
	private TextField red_ball_radius;
	@FXML
	private TextField green_ball_radius;
	@FXML
	private Text red_calibrated;
	@FXML
	private Text green_calibrated;
	@FXML
	private Label instruction;
	// a timer for acquiring the video stream
	private Timer timer1;
	// a timer for acquiring the video stream
	private Timer timer2;
	// the OpenCV object that performs the video capture
	private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive;
	//Imageviews for legend
	
	
	Mat frame = new Mat();
		
	Point final_center_red      = new Point();
	Point final_center_green      = new Point();
		
	private ObjectProperty<String> instructionProp;
	
    Scalar green = new Scalar(0,255,0);
	Scalar red = new Scalar(0,0,255);
	Scalar blue = new Scalar(255,0,0);
	Scalar white = new Scalar(255,255,255);
	Scalar black = new Scalar(0,0,0);
	Scalar color_ballToDraw = red;
	int ball_radius_fixed=40;
	Boolean red_detect_status=false;
	Boolean green_detect_status=false;
	Boolean red_calibrate_status=false;
	Boolean green_calibrate_status=false;
	Point red_ball_center_fixed= new Point(220,240);
	Point green_ball_center_fixed= new Point(420,240);
	Point red_ball_center= new Point(0,0);
	Point green_ball_center= new Point(0,0);
	int roundedRadius=0;
	Scalar red_HSV= new Scalar(255,255,255);
	
	
	static Scalar minValues_red = new Scalar(255,255,255);
	static Scalar maxValues_red = new Scalar(255,255,255);
	static Scalar minValues_green = new Scalar(255,255,255);
	static Scalar maxValues_green = new Scalar(255,255,255);
	
	String instructionText= "Start Calibration";
			
	/**
	 * The action triggered by pushing the button on the GUI
	 */
	@FXML
	private void recordParams()
	{	

		instructionProp = new SimpleObjectProperty<>();
		this.instruction.textProperty().bind(instructionProp);
	

		// bind an image property with the original frame container
		final ObjectProperty<Image> imageProp = new SimpleObjectProperty<>();
		this.originalFrame.imageProperty().bind(imageProp);
		
		
		
		// set a fixed width for all the image to show and preserve image ratio
		this.imageViewProperties(this.originalFrame, 800);
		
		if (!this.cameraActive)
		{
			// start the video capture
			this.capture.open(0);
			
			// is the video stream available?
			if (this.capture.isOpened())
			{
				this.cameraActive = true;
				
				// grab a frame every 33 ms (30 frames/sec)
				TimerTask frameGrabber = new TimerTask() {
					@Override
					public void run()
					{
						// update the image property => update the frame
						// shown in the UI
						Image frame = grabFrame(red);
						onFXThread(imageProp, frame);
					}
				};
				this.timer1 = new Timer();
				this.timer1.schedule(frameGrabber, 0, 33);
				
				// update the button content
				this.calibrateButton.setText("Calibrate Green");
				this.calibrateButton.setDisable(true);
			}
			else
			{
				// log the error
				System.err.println("Failed to open the camera connection...");
			}
		}
		else if(calibrateButton.getText()=="Calibrate Green")
		{
			timer1.cancel();
			this.timer1 = null;
//			timer1.purge();
			// grab a frame every 33 ms (30 frames/sec)
			TimerTask frameGrabber = new TimerTask() {
				@Override
				public void run()
				{
					// update the image property => update the frame
					// shown in the UI
					Image frame = grabFrame(green);
					onFXThread(imageProp, frame);
				}
			};
			this.timer2 = new Timer();
			this.timer2.schedule(frameGrabber, 0, 33);
			
			// update the button content
			this.calibrateButton.setText("Go to Application");
			this.calibrateButton.setDisable(true);
			
			calibrateButton.setOnAction(new EventHandler<ActionEvent>() {
			    public void handle(ActionEvent event) {
			        BorderPane root;
			        try {
			            root = FXMLLoader.load(getClass().getResource("ObjRecognition.fxml"));
			            Stage stage = new Stage();
			            stage.setTitle("Detect balls");
			            stage.setScene(new Scene(root, 1200, 800));
			            stage.show();
	
			            //hide this current window (if this is what you want
			            (((Node) event.getSource())).getScene().getWindow().hide();
			            timer2.cancel();
						timer2 = null;
						cameraActive = false;
						// release the camera
						capture.release();

			        } catch (IOException e) {
			            e.printStackTrace();
			        }
			    }
			});
		}
//		else {
//			timer2.cancel();
//			this.timer2 = null;
//			System.out.println("Timer2 is cancelled");
//			// the camera is not active at this point
//				this.cameraActive = false;
//				// update again the button content
////						this.calibrateButton.setText("Start Camera");
//				//Save graphs to files
////				Imgcodecs.imwrite("graph_1_"+new Date().getHours()+new Date().getMinutes()+".png", graph_1);
////				Imgcodecs.imwrite("graph_2_"+new Date().getHours()+new Date().getMinutes()+".png", graph_2);
//				// release the camera
//				this.capture.release();
//		}
	}
	
	/**
	 * Get a frame from the opened video stream (if any)
	 * 
	 * @return the {@link Image} to show
	 */
	

	private Image grabFrame(Scalar color)
	{
		// init everything
		Image imageToShow = null;
		
		
		// check if the capture is open
		if (this.capture.isOpened())
		{
			try
			{
				// read the current frame
				this.capture.read(frame);
				
				// if the frame is not empty, process it
				if (!frame.empty())
				{
					if(color==red){
					// init
					Mat blurredImage = new Mat();
					Mat hsvImage = new Mat();
					Mat mask_red = new Mat();
					Mat morphOutput_red = new Mat();
					
					// remove some noise
					Imgproc.blur(frame, blurredImage, new Size(7, 7));
					
					// convert the frame to HSV
					Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);
					
					// get thresholding values from the UI
					Scalar minValues_red = new Scalar(0,110,20);
					Scalar maxValues_red = new Scalar(5,230,255);
					
					// threshold HSV image to select tennis balls
					Core.inRange(hsvImage, minValues_red, maxValues_red, mask_red);

					// morphological operators
					// dilate with large element, erode with small ones
					Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
					Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12, 12));
					
					Imgproc.erode(mask_red, morphOutput_red, erodeElement);
					Imgproc.erode(mask_red, morphOutput_red, erodeElement);
					
					Imgproc.dilate(mask_red, morphOutput_red, dilateElement);
					Imgproc.dilate(mask_red, morphOutput_red, dilateElement);
					
					//Draw red OR green ball
					frame=drawBallForCalibration(red,frame,red_ball_center_fixed);
					// find the ball(s) contours and show them
					frame = this.findAndDrawRedBalls(morphOutput_red, frame);
					
					//Check whether ball is in place for calibration
					if (red_calibrate_status==false){
					if(red_ball_center.x>red_ball_center_fixed.x-5 && red_ball_center.x<red_ball_center_fixed.x+5){
						if(red_ball_center.y>red_ball_center_fixed.y-5 && red_ball_center.y<red_ball_center_fixed.y+5){
							if(roundedRadius<=ball_radius_fixed+3 && roundedRadius>=ball_radius_fixed-3){
								red_calibrate_status =true;
//								Imgproc.putText(frame, "", new Point(200,50), 0 , 1, new Scalar(0,200,200), 2);
								roundedRadius=40;
								red_calibrated.setVisible(true);
								set_HSV_Values_Red(frame,red_ball_center);
								this.calibrateButton.setDisable(false);
								instructionText = "Red ball is calibrated. Proceed to green ball";
							}
						}
					}
					}
			
				}
					else{
						// init
						Mat blurredImage = new Mat();
						Mat hsvImage = new Mat();
						Mat mask_green = new Mat();
						Mat morphOutput_green = new Mat();
						
						// remove some noise
						Imgproc.blur(frame, blurredImage, new Size(7, 7));
						
						// convert the frame to HSV
						Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);
						
						// get thresholding values from the UI
						Scalar minValues_green = new Scalar(42,47,20);
						Scalar maxValues_green = new Scalar(83,255,144);
					
						// threshold HSV image to select tennis balls
						Core.inRange(hsvImage, minValues_green, maxValues_green, mask_green);
						
						// morphological operators
						// dilate with large element, erode with small ones
						Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
						Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12,12));
						
						Imgproc.erode(mask_green, morphOutput_green, erodeElement);
						Imgproc.erode(mask_green, morphOutput_green, erodeElement);
						
						Imgproc.dilate(mask_green, morphOutput_green, dilateElement);
						Imgproc.dilate(mask_green, morphOutput_green, dilateElement);
						
						//Draw green ball
						frame=drawBallForCalibration(green,frame,green_ball_center_fixed);
						// find the ball(s) contours and show them
						frame = this.findAndDrawGreenBalls(morphOutput_green, frame);
						
						//Check whether ball is in place for calibration
						if (green_calibrate_status==false){
						if(green_ball_center.x>green_ball_center_fixed.x-5 && green_ball_center.x<green_ball_center_fixed.x+5){
							if(green_ball_center.y>green_ball_center_fixed.y-5 && green_ball_center.y<green_ball_center_fixed.y+5){
								if(roundedRadius<=ball_radius_fixed+3 && roundedRadius>=ball_radius_fixed-3){
									green_calibrate_status =true;
//									Imgproc.putText(frame, "", new Point(200,50), 0 , 1, new Scalar(0,200,200), 2);
									roundedRadius=40;
									green_calibrated.setVisible(true);
									set_HSV_Values_Green(frame,green_ball_center);
									this.calibrateButton.setDisable(false);
									instructionText = "Both balls are calibrated. Proceed to application";
								}
							}
						}
						}
					}
					this.onFXThread(instructionProp, instructionText);
					imageToShow = mat2Image(frame);
				}
				
			}
			catch (Exception e)
			{
				// log the (full) error
				System.err.print("ERROR");
				e.printStackTrace();
			}
		}
		
		return imageToShow;
	}
	
	private void imageViewProperties(ImageView image, int dimension)
	{
		// set a fixed width for the given ImageView
		image.setFitWidth(dimension);
		// preserve the image ratio
		image.setPreserveRatio(true);
	}
		
	private Image mat2Image(Mat frame)
	{
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		// encode the frame in the buffer, according to the PNG format
		Imgcodecs.imencode(".png", frame, buffer);
		// build and return an Image created from the image encoded in the
		// buffer
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}
		
	private <T> void onFXThread(final ObjectProperty<T> property, final T value)
	{
		Platform.runLater(new Runnable() {
			
			@Override
			public void run()
			{
				property.set(value);
				
			}
		});
	}
	
	private Mat findAndDrawRedBalls(Mat maskedImage, Mat frame)
	{
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		int largestCount=0;
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

	    Point center      = new Point();
	    float[] radius     = new float[contours.size()];
	    
	    double area;
	    double prev_area=0;
	   	//if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			red_detect_status=true;

				for (int i = 0; i < contours.size(); i++) {
					//Detect circles with inbuilt function
			        area = Imgproc.contourArea(new MatOfPoint(contours.get(i)));
			        if (area > prev_area){
			        	prev_area = area;
			        	largestCount = i;
			        	final_center_red = center;
			        }
			        
			    }
				prev_area=0;
		        Imgproc.minEnclosingCircle(new MatOfPoint2f(contours.get(largestCount).toArray()), center, radius);
		        if (red_calibrate_status==false){
		        roundedRadius = (int) Math.round(0.65*radius[0]);
		        }
				Imgproc.circle(frame, new Point(final_center_red.x,final_center_red.y), roundedRadius, red,2);
				
		}
		
//		Imgproc.putText(frame, touch_status1, new Point(50,450), 0 , 1, new Scalar(0,200,200), 2);
		red_ball_center.x = final_center_red.x;
		red_ball_center.y = final_center_red.y;
		return frame;
	}

	private Mat findAndDrawGreenBalls(Mat maskedImage, Mat frame)
	{
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		int largestCount=0;
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

	    Point center      = new Point();
	    float[] radius     = new float[contours.size()];
	    double area;
	    double prev_area=0;
	   	//if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			green_detect_status=true;

				for (int i = 0; i < contours.size(); i++) {
					//Detect circles with inbuilt function
			        area = Imgproc.contourArea(new MatOfPoint(contours.get(i)));
			        if (area > prev_area){
			        	prev_area = area;
			        	largestCount = i;
			        	final_center_green = center;
			        }
			        
			    }
				prev_area=0;
				
		        Imgproc.minEnclosingCircle(new MatOfPoint2f(contours.get(largestCount).toArray()), center, radius);
		        if (green_calibrate_status==false){
			        roundedRadius = (int) Math.round(0.65*radius[0]);
			        }
				Imgproc.circle(frame, new Point(final_center_green.x,final_center_green.y), roundedRadius,green,2);
		}
		green_ball_center.x = final_center_green.x;
		green_ball_center.y = final_center_green.y;
	
		return frame;
	}

	
	private Mat drawBallForCalibration(Scalar color, Mat frame, Point center){
		Imgproc.circle(frame, center, ball_radius_fixed, color,4);
		return frame;
	}
	
	private void set_HSV_Values_Red(Mat frame,Point center){
		Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2HSV);
		int[] H =new int[25];	
		int[] S =new int[25];	
		int[] V =new int[25];	
		
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				H[j+i*5]=(int)frame.get((int)(center.x+5*(i-2)),(int)center.y+5*(j-2))[0];
				S[j+i*5]=(int)frame.get((int)(center.x+5*(i-2)),(int)center.y+5*(j-2))[1];
				V[j+i*5]=(int)frame.get((int)(center.x+5*(i-2)),(int)center.y+5*(j-2))[2];

			}
		}
		Arrays.sort(H);
		Arrays.sort(S);
		Arrays.sort(V);

		for(int i=1;i<H.length;i++){
			if(H[H.length-i]<10){
				maxValues_red=new Scalar(H[H.length-i],S[S.length-i]+50,V[V.length-i]+50); 
				minValues_red=new Scalar(H[0],S[0]+50,V[0]-50);
				break;
			}
		}
		HSV_red_max.setText("("+String.valueOf(maxValues_red.val[0])+","+String.valueOf(maxValues_red.val[1])+
				","+String.valueOf(maxValues_red.val[2])+")");
		HSV_red_min.setText("("+String.valueOf(minValues_red.val[0])+","+String.valueOf(minValues_red.val[1])+
				","+String.valueOf(minValues_red.val[2])+")");
		red_ball_radius.setText(String.valueOf(ball_radius_fixed));
		HSV_red_max.setEditable(false);
		HSV_red_min.setEditable(false);

	}
	
	private void set_HSV_Values_Green (Mat frame,Point center){
		Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2HSV);
		double[][] HSV =new double[25][3];	

		
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				HSV[j+i*5]=frame.get((int)(center.x+5*(i-2)),(int)center.y+5*(j-2));

			}
		}

		Mat cropped = new Mat(frame, new Rect(new Point(center.x-10,center.y-10),new Point(center.x+10,center.y+10)));
		Scalar mean=Core.mean(cropped);
		
		maxValues_green = new Scalar(mean.val[0]+10,mean.val[1]+50,mean.val[2]+50);
		minValues_green = new Scalar(mean.val[0]-10,mean.val[1]-50,mean.val[2]-50);
		HSV_green_max.setText("("+String.valueOf(Math.round(maxValues_green.val[0]))+
				","+String.valueOf(Math.round(maxValues_green.val[1]))+
				","+String.valueOf(Math.round(maxValues_green.val[2]))+")");
		HSV_green_min.setText("("+String.valueOf(Math.round(minValues_green.val[0]))+","
				+String.valueOf(Math.round(minValues_green.val[1]))+
				","+String.valueOf(Math.round(minValues_green.val[2]))+")");
		green_ball_radius.setText(String.valueOf(ball_radius_fixed));
		HSV_green_max.setEditable(false);
		HSV_green_min.setEditable(false);

	}
}