package application;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.beans.value.ChangeListener;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Toggle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.*;
import org.opencv.videoio.VideoCapture;
import org.opencv.imgproc.Imgproc;

/**
 * The controller associated with the calibration part of the app
 * Calibration is done for detecting color as well as actual size
 * Correct color calibration enables better detection in the actual application
 * Actual size detection enable a plot of angle vs distance
 */
public class CalibrationController2
{
	// FXML camera button
	@FXML
	private Button calibrateButton;
	@FXML
	private Button calibrateActionButton;
	@FXML
	private Button calibrateActionButton1;
	// the FXML area for showing the current frame
	@FXML
	private ImageView originalFrame;
	@FXML
	private ImageView second;
	@FXML
	private ImageView third;
	//Text and static components from the FXML
	@FXML
	private TextField HSV_red_min;
	@FXML
	private TextField HSV_red_max;
	@FXML
	private TextField HSV_green_min;
	@FXML
	private TextField HSV_green_max;
	@FXML
	private TextField red_ball_radius;
	@FXML
	private TextField green_ball_radius;
	@FXML
	private Text red_calibrated;
	@FXML
	private Text green_calibrated;
	@FXML
	private Label instruction;
	// a timer for acquiring the video stream
	private Timer timer;
	// the OpenCV object that performs the video capture
	private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive;
	
	
	Mat frame = new Mat();
		
	Point final_center_red      = new Point();
	Point final_center_green      = new Point();
		
	private ObjectProperty<String> instructionProp;
	
    Scalar green = new Scalar(0,255,0);
	Scalar red = new Scalar(0,0,255);
	Scalar blue = new Scalar(255,0,0);
	Scalar white = new Scalar(255,255,255);
	Scalar black = new Scalar(0,0,0);
	Scalar color_ballToDraw = red;
	int ball_radius_fixed=40;
	Boolean red_detect_status=false;
	Boolean green_detect_status=false;
	Boolean red_calibrate_status=false;
	Boolean green_calibrate_status=false;
	Point red_ball_center_fixed= new Point(220,240);
	Point green_ball_center_fixed= new Point(420,240);
	Point red_ball_center= new Point(0,0);
	Point green_ball_center= new Point(0,0);
	int roundedRadius=0;
	Scalar red_HSV= new Scalar(255,255,255);
	
	
	static Scalar minValues_red = new Scalar(255,255,255);
	static Scalar maxValues_red = new Scalar(255,255,255);
	static Scalar minValues_green = new Scalar(255,255,255);
	static Scalar maxValues_green = new Scalar(255,255,255);
	
	String instructionText= "Step 1 : Fit the Red ball in the RED circle and hit 'Calibrate'";
			
	/**
	 * The action triggered by pushing the START CALIBRATION button on the GUI
	 */
	
	@FXML
	private void startCalibration()
	{	

		instructionProp = new SimpleObjectProperty<>();
		this.instruction.textProperty().bind(instructionProp);
	
		// bind an image property with the original frame container
		final ObjectProperty<Image> imageProp = new SimpleObjectProperty<>();
		this.originalFrame.imageProperty().bind(imageProp);
		
		// bind an image property with the first histogram container
		final ObjectProperty<Image> secondProp = new SimpleObjectProperty<>();
		this.second.imageProperty().bind(secondProp);
		
		// bind an image property with the second histogram container
		final ObjectProperty<Image> thirdProp = new SimpleObjectProperty<>();
		this.third.imageProperty().bind(thirdProp);
				
		// set a fixed width for all the image to show and preserve image ratio
		this.imageViewProperties(this.originalFrame, 800);
		this.imageViewProperties(this.second, 200);
		this.imageViewProperties(this.third, 200);
		
		if (!this.cameraActive)
		{
			// start the video capture
			this.capture.open(0);
			
			// is the video stream available?
			if (this.capture.isOpened())
			{
				this.cameraActive = true;
				
				// grab a frame every 33 ms (30 frames/sec)
				TimerTask frameGrabber = new TimerTask() {
					@Override
					public void run()
					{
						// update the image property => update the frame
						// shown in the UI
						Image frame = grabFrame();
						onFXThread(imageProp, frame);
					}
				};
				this.timer = new Timer();
				this.timer.schedule(frameGrabber, 0, 33);
				
				// update the button content
				this.calibrateButton.setText("Go To Application");
				
				//disable the button until the calibration is done
				this.calibrateButton.setDisable(true);
				
				//set other text etc visible
				this.instruction.setVisible(true);
				this.calibrateActionButton.setVisible(true);
				this.calibrateActionButton1.setVisible(true);
				
				//action set on CALIBRATE RED button
				calibrateActionButton.setOnAction(new EventHandler<ActionEvent>() {
				    public void handle(ActionEvent event) {				    	
					        
				    	// get color calibration values from RED ball frame histogram
				    	Image newframe = getColorFromHistoGram(red_ball_center_fixed,frame,red);
					        onFXThread(secondProp, newframe);
					        instructionText = "Step 2 : Fit the Green ball in the GREEN circle and hit 'Calibrate'"
					        		+ "OR re-calibrate red ball";
					        red_calibrate_status=true;
					        
					        //if both balls are detected, enable the GO TO APPLICATION button
					        if (red_calibrate_status==true && green_calibrate_status==true){
								calibrateButton.setDisable(false);
							}
					        
					        //show calibrated values in textbox
					        HSV_red_max.setText("("+String.valueOf(maxValues_red.val[0])+","
					        		+String.valueOf(maxValues_red.val[1])+
									","+String.valueOf(maxValues_red.val[2])+")");
							HSV_red_min.setText("("+String.valueOf(minValues_red.val[0])+","
									+String.valueOf(minValues_red.val[1])+
									","+String.valueOf(minValues_red.val[2])+")");
							red_ball_radius.setText(String.valueOf(ball_radius_fixed));
				    }
				});
				
				//action set on CALIBRATE GREEN button
				calibrateActionButton1.setOnAction(new EventHandler<ActionEvent>() {
				    public void handle(ActionEvent event) {				    	
					        
				    	// get color calibration values from GREEN ball frame histogram
			    		Image newframe = getColorFromHistoGram(green_ball_center_fixed,frame,green);
				        onFXThread(thirdProp, newframe);
				        instructionText = "Step 3 : Both balls are calibrated. Proceed to application"
				        		+ " OR re-calibrate either ball";
				        green_calibrate_status=true;
				        
				        //if both balls are detected, enable the GO TO APPLICATION button
				        if (red_calibrate_status==true && green_calibrate_status==true){
							calibrateButton.setDisable(false);
						}
				        
				        //show calibrated values in textbox
				        HSV_green_max.setText("("+String.valueOf(Math.round(maxValues_green.val[0]))+
									","+String.valueOf(Math.round(maxValues_green.val[1]))+
									","+String.valueOf(Math.round(maxValues_green.val[2]))+")");
						HSV_green_min.setText("("+String.valueOf(Math.round(minValues_green.val[0]))+","
								+String.valueOf(Math.round(minValues_green.val[1]))+
								","+String.valueOf(Math.round(minValues_green.val[2]))+")");
						green_ball_radius.setText(String.valueOf(ball_radius_fixed));
				    }
				});
				
				//action set on GO TO APPLICATION button
				calibrateButton.setOnAction(new EventHandler<ActionEvent>() {
				    public void handle(ActionEvent event) {
				        BorderPane root;
				        try {
				            root = FXMLLoader.load(getClass().getResource("ObjRecognition.fxml"));
				            Stage stage = new Stage();
				            stage.setTitle("Detect balls");
				            Scene scene = new Scene(root, 1200, 800);
//				            scene.setFill(null);
				            stage.setScene(scene);
				            
				            stage.show();
				            stage.setFullScreen(true);
		
				            //hide this current window
				            (((Node) event.getSource())).getScene().getWindow().hide();
				            timer.cancel();
							timer = null;
							cameraActive = false;
							// release the camera
							capture.release();

				        } catch (IOException e) {
				            e.printStackTrace();
				        }
				    }
				});
				
			}
			else
			{
				// log the error
				System.err.println("Failed to open the camera connection...");
			}
		}	

	}
	
	/**
	 * Get a frame from the opened video stream (if any)
	 * 
	 * @return the {@link Image} to show
	 */
	

	private Image grabFrame()
	{
		// init everything
		Image imageToShow = null;
		
		
		// check if the capture is open
		if (this.capture.isOpened())
		{
			try
			{
				// read the current frame
				this.capture.read(frame);
				
				// if the frame is not empty, process it
				if (!frame.empty())
				{													
					//Draw red calibration circle
					frame=drawBallForCalibration(black,frame,red_ball_center_fixed);
																
					//Draw green calibration circle
					frame=drawBallForCalibration(black,frame,green_ball_center_fixed);						
							
					this.onFXThread(instructionProp, instructionText);
					Imgproc.putText(frame, "RED", new Point(red_ball_center_fixed.x-30, red_ball_center_fixed.y+100), 0 , 1, 
							white, 2);
					Imgproc.putText(frame, "GREEN", new Point(green_ball_center_fixed.x-50, green_ball_center_fixed.y+100), 0 , 1, 
							white, 2);
					imageToShow = mat2Image(frame);
					
				}
				
			}
			catch (Exception e)
			{
				// log the (full) error
				System.err.print("ERROR");
				e.printStackTrace();
			}
		}
		return imageToShow;		
		
	}
	
	private void imageViewProperties(ImageView image, int dimension)
	{
		// set a fixed width for the given ImageView
		image.setFitWidth(dimension);
		// preserve the image ratio
		image.setPreserveRatio(true);
	}
		
	private Image mat2Image(Mat frame)
	{
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		// encode the frame in the buffer, according to the PNG format
		Imgcodecs.imencode(".png", frame, buffer);
		// build and return an Image created from the image encoded in the
		// buffer
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}
		
	private <T> void onFXThread(final ObjectProperty<T> property, final T value)
	{
		Platform.runLater(new Runnable() {
			
			@Override
			public void run()
			{
				property.set(value);
				
			}
		});
	}
	
	// This function crops the area around the calibration ball
	// mask it with a circle to show only the circular calibration region
	// calculates the histogram of the cropped image to find out the majority H, S, V values
	// with the help of minMaxSweep function
	
	private Image getColorFromHistoGram(Point center,Mat frame,Scalar color){
		Mat cropped = new Mat(frame, new Rect(new Point(center.x-50,center.y-50),new Point(center.x+50,center.y+50)));
		int croppedImageType = cropped.type();
		
		// square image containing the calibration circle
		Mat toBeHisto= new Mat(100, 100, croppedImageType, new Scalar(0,0,0));
		
		//circular mask to reveal only the calibration circle content
		Imgproc.circle(toBeHisto, new Point(ball_radius_fixed+10, ball_radius_fixed+10), ball_radius_fixed, white,-1);
		Mat masked = new Mat();
		Core.bitwise_and(cropped, toBeHisto, masked);
		
		//converted to HSV to calculate HSV values
		Imgproc.cvtColor(masked, masked, Imgproc.COLOR_BGR2HSV);		
		
		// split the frames in multiple images
		List<Mat> images = new ArrayList<Mat>();
		Core.split(masked, images);
		
		
		// set the number of bins - no. of bins equal no. of values
		MatOfInt histSize_h = new MatOfInt(180); //180 for HUE
		MatOfInt histSize_sv = new MatOfInt(256); // 255 for SATURATION and VALUE
		// only one channel
		MatOfInt channels = new MatOfInt(0);
		// set the ranges
		MatOfFloat histRange_h = new MatOfFloat(0, 179);
		MatOfFloat histRange_sv = new MatOfFloat(0, 256);
		
		// compute the histograms for the B, G and R components
		Mat hist_h = new Mat();
		Mat hist_s = new Mat();
		Mat hist_v = new Mat();
		
		
		// calculate H, S, V component's histogram
		Imgproc.calcHist(images.subList(0, 1), channels, new Mat(), hist_h, histSize_h, histRange_h, false);
		Imgproc.calcHist(images.subList(1, 2), channels, new Mat(), hist_s, histSize_sv, histRange_sv, false);
		Imgproc.calcHist(images.subList(2, 3), channels, new Mat(), hist_v, histSize_sv, histRange_sv, false);
		
		//modified MAT with first element removed (first element corresponds to 0 HUE i.e. black, which is the
		// unmasked portion of the cropped image)
		Mat hist_h_mod = new Mat();
		Mat hist_s_mod = new Mat();
		Mat hist_v_mod = new Mat();
		
		for(int i=1;i<=hist_h.rows()-1;i++){
			hist_h_mod.push_back(hist_h.row(i));
		}
		for(int i=1;i<=hist_s.rows()-1;i++){
			hist_s_mod.push_back(hist_s.row(i));
			hist_v_mod.push_back(hist_v.row(i));
		}			
		
		// draw the histogram
		int hist_wd = 200; // width of the histogram image
		int hist_ht = 200; // height of the histogram image
		int bin_w_h = (int) Math.round(hist_wd / histSize_h.get(0, 0)[0]);
		int bin_w_sv = (int) Math.round(hist_wd / histSize_sv.get(0, 0)[0]);

		Mat histImage = new Mat(hist_ht, hist_wd, CvType.CV_8UC3, new Scalar(0, 0, 0));

		// normalize the result to [0, histImage.rows()]
		Core.normalize(hist_h_mod, hist_h_mod, 0, 200, Core.NORM_MINMAX, -1, new Mat());
		Core.normalize(hist_s_mod, hist_s_mod, 0, 200, Core.NORM_MINMAX, -1, new Mat());
		Core.normalize(hist_v_mod, hist_v_mod, 0, 200, Core.NORM_MINMAX, -1, new Mat());

		
		// effectively draw the histogram(s)
		for (int i = 1; i < histSize_h.get(0, 0)[0]-1; i++)
		{
			// H component 
			Imgproc.line(histImage, new Point(bin_w_h * (i - 1), hist_ht - Math.round(hist_h_mod.get(i - 1, 0)[0])),
					new Point(bin_w_h * (i), hist_ht - Math.round(hist_h_mod.get(i, 0)[0])), new Scalar(255, 0, 0), 2, 8, 0);
		}
		// S and V components 
		for (int i = 1; i < histSize_sv.get(0, 0)[0]-1; i++)
		{
			Imgproc.line(histImage, new Point(bin_w_sv * (i - 1), hist_ht - Math.round(hist_s_mod.get(i - 1, 0)[0])),
					new Point(bin_w_sv * (i), hist_ht - Math.round(hist_s_mod.get(i, 0)[0])), new Scalar(0, 255, 0), 2, 8,
					0);
			Imgproc.line(histImage, new Point(bin_w_sv * (i - 1), hist_ht - Math.round(hist_v_mod.get(i - 1, 0)[0])),
					new Point(bin_w_sv * (i), hist_ht - Math.round(hist_v_mod.get(i, 0)[0])), new Scalar(0, 0, 255), 2, 8,
					0);			
		}
		
		//Finding out the range of H values for Red color(0-180)
		int max_h_loc=(int) Core.minMaxLoc(hist_h_mod).maxLoc.y;
		int h_sum = (int) (Core.sumElems(hist_h_mod)).val[0];
		
		//Finding out the range of S values for Red color(0-255)
		int max_s_loc=(int) Core.minMaxLoc(hist_s_mod).maxLoc.y;
		int s_sum = (int) (Core.sumElems(hist_s_mod)).val[0];
		
		//Finding out the range of V values for Red color(0-255)
		int max_v_loc=(int) Core.minMaxLoc(hist_v_mod).maxLoc.y;
		int v_sum = (int) (Core.sumElems(hist_v_mod)).val[0];
		
		
		if(color ==red){
			minValues_red.val[0] = minMaxSweep(hist_h_mod,h_sum,max_h_loc,179,0).x;
			maxValues_red.val[0] = minMaxSweep(hist_h_mod,h_sum,max_h_loc,179,0).y;
			
			minValues_red.val[1] = minMaxSweep(hist_s_mod,s_sum,max_s_loc,254,1).x;
			maxValues_red.val[1] = minMaxSweep(hist_s_mod,s_sum,max_s_loc,254,1).y;
			
			
			minValues_red.val[2] = Math.max(minMaxSweep(hist_v_mod,v_sum,max_v_loc,254,2).x,20);
			maxValues_red.val[2] = minMaxSweep(hist_v_mod,v_sum,max_v_loc,254,2).y;
		}
		else {
			minValues_green.val[0] = minMaxSweep(hist_h_mod,h_sum,max_h_loc,179,0).x;
			maxValues_green.val[0] = minMaxSweep(hist_h_mod,h_sum,max_h_loc,179,0).y;
			
			minValues_green.val[1] = minMaxSweep(hist_s_mod,s_sum,max_s_loc,254,1).x;
			maxValues_green.val[1] = minMaxSweep(hist_s_mod,s_sum,max_s_loc,254,1).y;
			
			minValues_green.val[2] = Math.max(minMaxSweep(hist_v_mod,v_sum,max_v_loc,254,2).x,20);
			maxValues_green.val[2] = minMaxSweep(hist_v_mod,v_sum,max_v_loc,254,2).y;
		}
		
		return mat2Image(histImage);
	}
	
	/**
	 * 
	 * @param inputMat
	 * @param Sum
	 * @param Location
	 * @param range
	 * @param index
	 * @return
	 * Function finds the range of values in a histogram whose sum is greater than a certain % of sum of all values
	 * e.g. HUE values are from 0-179 in openCV. From the histogram we may find that HUE values of 1-6 contain
	 * more than 80% of the pixels of that image. This gives a definitive idea about the major color of the ball
	 */
	private Point minMaxSweep(Mat inputMat,Integer Sum, Integer Location, Integer range, Integer index){
		double sumOfTerms=0;
		int minValues=0;
		int maxValues=0;
		
		// the loop increases the range of values on both side of maximum value in the histogram
		// such that the sum of total no. of pixels having those values becomes more than 80% of total pixels
		thisloop:
		for(int factor=1;factor<range;factor=factor*2){
			
			int minLimit = Location-factor;
			int maxLimit = Location+factor;
			
			if (Location-factor<0){
				minLimit =0;
			}
			if (Location+factor>range){
				maxLimit =range;
			}
			
			if(sumOfTerms>=0.8*Sum){
				minValues = minLimit;
				maxValues = maxLimit;
				break thisloop;
			}
			else {		
				sumOfTerms=0;
				for(int i=minLimit;i<=maxLimit;i++){
					sumOfTerms=sumOfTerms+ inputMat.get(i,0)[0];
				}	
			}
			
		}
		Point values = new Point(minValues,maxValues);
		return values;
	}

	// draws colored circle on screen for calibration purpose
	private Mat drawBallForCalibration(Scalar color, Mat frame, Point center){
		Imgproc.circle(frame, center, ball_radius_fixed, color,1);
		return frame;
	}
	
}